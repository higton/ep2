package application;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import static application.Carro.*;
import static org.junit.jupiter.api.Assertions.*;

class CarroTest {
    public double cargaMax;
    public double velocidadeMedia;
    public double rendimentoFinal;

    @BeforeEach
    public void carro(){
        rendimentoFinal = 0;
        cargaMax = 360;
        velocidadeMedia = 100;
        Carro carro = new Carro(cargaMax, velocidadeMedia, rendimentoFinal);
    }

    @Test
    public void testaConstrutor() {
        Carro carro = new Carro(cargaMax, velocidadeMedia, rendimentoFinal);


    }


    @org.junit.jupiter.api.Test
    void excederTempo() {
        Carro carro = new Carro(360,100,0);
        Usuario.setDistanciaCarga(1200);
        Usuario.setTempoCarga(15);
        Carro.setVelocidadeMedia(75);
        double v;
        v= ( Usuario.getDistanciaCarga()/Usuario.getTempoCarga() );
        assertEquals(true, carro.excederTempo());
    }
    @org.junit.jupiter.api.Test
    void excederPeso() {
        Carro carro = new Carro(360,100,0);
        setCargaMax(5);
        Usuario.setPesoCarga(30);
        assertEquals(true, carro.excederPeso());
    }

    @org.junit.jupiter.api.Test
    void disponivel() {
        Carro carro = new Carro(360, 100, 0);
        Usuario.setQuantidadeCarros(0);
        assertEquals(false, carro.disponivel());
        carro.imprimirDados();
        carro.calcularRendimentoFinal();
    }

    @org.junit.jupiter.api.Test
    void test() {
        Carro carro = new Carro(360, 100, 0);
        Carro.setTipoCombustivel(2);
        Carro.setTempo(12);
        //Carro.setCusto(371);
        Carro.setVelocidadeMedia(100);
        Carro.setRendimentoFinal(1);
        Carro.setCargaMax(360);
        Carro.setDisponivel(true);
        carro.imprimirDados();
        Veiculo.setGasolina(4.449);
        Veiculo.setAlcool(3.499);
        Usuario.setDistanciaCarga(1200);
        Usuario.setQuantidadeCarros(3);
        Usuario.setDistanciaCarga(1200);
        Usuario.setTempoCarga(30);
        Usuario.setPesoCarga(30);
        Usuario.setQuantidadeCarros(3);
        carro.calcularRendimentoFinal();
        assertEquals(Carro.getTipoCombustivel(), 2);
        assertEquals(Carro.getTempo(), 12);
        assertEquals(Carro.getCusto(), 371.34518439904485);
        assertEquals(Carro.getVelocidadeMedia(), 100);
        assertEquals(Carro.getRendimentoFinal(), 11.307);
        assertEquals(Carro.getCargaMax(), 360);
        assertEquals(Carro.getDisponivel(), true);
    }
}