package application;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ControllerCena2 implements Initializable {
    @FXML
    private ListView listView;
    @FXML
    private Button BtnDelete;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //TODO
        listView.setItems(listItems);
        //mudando o tamanho da letra no listview
        listView.setStyle("-fx-font-size: 16px; -fx-font-family: 'SketchFlow Print';");
        //Colocando testo na listView
        System.out.println("lendo arquivo");
        try {
            BufferedReader reader = new BufferedReader(new FileReader("demandas.txt"));
            String line = reader.readLine();
            int i=0;
            while (line != null) {
                i++;
                line.trim().startsWith("0.0");
                String[] data = line.split(","); // since your delimiter is ","
                if(line!=null) {
                    System.out.println(data[0]);
                    listView.getItems().add("Distancia: " + data[0] + " kilometros, peso da carga: " + data[1] + " kilogramas , tempo: "
                            + data[2] + " horas, " + "margem de lucro: " + data[3] + "%, o veículo escolhido é: " + data[4]);
                    //ler a proxima linha

                    line = reader.readLine();
                }
            }

            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    final ObservableList<String> listItems = FXCollections.observableArrayList("Add Items here");

    // mudar de cena
    public void goScene3(ActionEvent event) throws IOException {
        //Ler o arquivo para atualizar a lista de frotas, depois de uma demanda ser deletada ou não


        Parent tableViewParent = FXMLLoader.load(getClass().getResource("cena3.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }

    //Quando um item for selecionado ele vai ser deletado
    @FXML
    private void deleteAction(ActionEvent action)throws IOException{
        int selectedItem = listView.getSelectionModel().getSelectedIndex();
        System.out.println("o item selecionado é " + selectedItem);
        Arquivo.delete(selectedItem);
        listItems.remove(selectedItem);
    }
}
