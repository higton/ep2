package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ControllerCena1 implements Initializable {
    @FXML //fx:id="text-field";
    private TextField text_field1;
    @FXML private TextField text_field2;
    @FXML private TextField text_field3;
    @FXML private TextField text_field4;
    @FXML private TextField text_field5;
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        this.text_field1.setText("" + Usuario.getQuantidadeMotos());
        this.text_field2.setText("" + Usuario.getQuantidadeCarretas());
        this.text_field3.setText("" + Usuario.getQuantidadeCarros());
        this.text_field4.setText("" + Usuario.getQuantidadeVans());
        this.text_field5.setText("" + Usuario.getMargemLucro());
    }
    //Trocando de telas
    @FXML
    private void goScene2(ActionEvent event) throws IOException {
        double numero1 = Double.parseDouble(this.text_field1.getText());
        double numero2 = Double.parseDouble(this.text_field2.getText());
        double numero3 = Double.parseDouble(this.text_field3.getText());
        double numero4 = Double.parseDouble(this.text_field4.getText());
        Usuario.setQuantidadeMotos(numero1);
        Usuario.setQuantidadeCarretas(numero2);
        Usuario.setQuantidadeCarros(numero3);
        Usuario.setQuantidadeVans(numero4);

        String palavra = this.text_field5.getText();
        Usuario.setMargemLucro(Double.parseDouble(palavra));
        System.out.println("Addscene o valor da margem de lucro e " + Usuario.getMargemLucro());

        //Escrever no arquivo as mudanças da cena1
        Arquivo.escreverArquivo();

        Parent view = FXMLLoader.load(getClass().getResource("cena2.fxml"));
        Scene scene = new Scene(view);
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window.setScene(scene);
        window.show();
    }

    //Quando apertar o botão de mais ou menos
    @FXML
    protected void aumentarMoto() {
        System.out.println("Aumentar o numero de moto");
        Usuario.adicionarMoto();
        this.text_field1.setText("" + Usuario.getQuantidadeMotos());
    }

    @FXML
    protected void diminuirMoto() {
        System.out.println("Diminuir o numero de moto");
        Usuario.removerMoto();
        this.text_field1.setText("" + Usuario.getQuantidadeMotos());
    }

    @FXML
    protected void aumentarCarreta() {
        System.out.println("Aumentar o numero de carreta");
        Usuario.adicionarCarreta();
        this.text_field2.setText("" + Usuario.getQuantidadeCarretas());
    }

    @FXML
    protected void diminuirCarreta() {
        System.out.println("Diminuir o numero de carreta");
        Usuario.removerCarreta();
        this.text_field2.setText("" + Usuario.getQuantidadeCarretas());
    }

    @FXML
    protected void aumentarCarros() {
        System.out.println("Aumentar o numero de carros");
        Usuario.adicionarCarro();
        this.text_field3.setText("" + Usuario.getQuantidadeCarros());
    }

    @FXML
    protected void diminuirCarros() {
        System.out.println("Diminuir o numero de carros");
        Usuario.removerCarro();
        this.text_field3.setText("" + Usuario.getQuantidadeCarros());
    }

    @FXML
    protected void aumentarVans() {
        System.out.println("Aumentar o numero de vans");
        Usuario.adicionarVan();
        this.text_field4.setText("" + Usuario.getQuantidadeVans());
    }

    @FXML
    protected void diminuirVans() {
        Usuario.removerVan();
        this.text_field4.setText("" + Usuario.getQuantidadeVans());

    }
}
