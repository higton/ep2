package application;

public class Carro extends Veiculo{
	private static double velocidadeMedia;
	private static double rendimentoFinal;
	private static double cargaMax;
	private static double custo;
	private static double custoBeneficio;
	private static double tempo;
	private static boolean disponivel;
	private static int tipoCombustivel;

	//metodo acessores
	public static int getTipoCombustivel() { return tipoCombustivel; }
	public static void setTipoCombustivel(int tipoCombustivel) { Carro.tipoCombustivel = tipoCombustivel; }
	public static boolean getDisponivel() { return disponivel; }
	public static void setDisponivel(boolean disponivel) { Carro.disponivel = disponivel; }
	public static double getTempo() { return tempo; }
	public static void setTempo(double tempo) { Carro.tempo = tempo; }
	public static double getCusto() { return custo; }
	public static void setCusto(double custo) { Carro.custo = custo; }
	public static double getRendimentoFinal() {
		return rendimentoFinal;
	}
	public static double getCargaMax() {
		return cargaMax;
	}
	public static void setCargaMax(double cargaMax) {
		Carro.cargaMax = cargaMax;
	}
	public static void setRendimentoFinal(double rendimentoFinal) {
		Carro.rendimentoFinal = rendimentoFinal;
	}
	public static double getVelocidadeMedia() {
		return velocidadeMedia;
	}
	public static void setVelocidadeMedia(double velocidadeMedia) {
		Carro.velocidadeMedia = velocidadeMedia;
	} // fim dos metodos acessores
	public Carro(double cargaMax , double velocidadeMedia, double rendimentoFinal) {
		setCargaMax(cargaMax);
		setVelocidadeMedia(velocidadeMedia);
		setRendimentoFinal(rendimentoFinal);
	}
	public boolean excederTempo() {
		double v;
		v= ( Usuario.getDistanciaCarga()/Usuario.getTempoCarga() );
		if( v <= getVelocidadeMedia() ) {
			Carro.setDisponivel(true);
			return false;
		}
		else {
			return true;
		}
	}
	public boolean excederPeso() {
		if(getCargaMax()<Usuario.getPesoCarga()) {
			return true;
		}
		else {
			Carro.setDisponivel(true);
			return false;
		}
	}
	@Override
	public boolean disponivel(){

		if(Usuario.getQuantidadeCarros() != 0){
			Carro.setDisponivel(true);
			return true;
		}
		else{
			Carro.setDisponivel(false);
			return false;
		}
	}
	@Override
	public void calcularRendimentoFinal() {
		double a = 0;
		double rendimento1 = 0;
		double rendimento2 = 0;
		double litros1, litros2, custo1, custo2;
		//Se o combustivel é gasolina
		if(!excederTempo() && !excederPeso() && disponivel()) {
			setCombustivel("Gasolina"); // combustivel 1
			setRendimento(14);
			a = Usuario.getPesoCarga();
			rendimento1 = getRendimento() - a*0.025;
		}
		//Se o combustível é álcool
		if(!excederTempo() && !excederPeso() && disponivel()) {
			setCombustivel("Alcool"); //combustivel 2
			setRendimento(12);
			a = Usuario.getPesoCarga();
			rendimento2 = getRendimento() - a*0.0231;
		}
		//Escolhendo o combustível
		litros1 = Usuario.getDistanciaCarga()/rendimento1;
		litros2 = Usuario.getDistanciaCarga()/rendimento2;
		custo1 = litros1*getGasolina();
		custo2 = litros2*getAlcool();
		if(custo1 == 0 || custo2 == 0) {
			setTipoCombustivel(0);
		}
		if(custo1>custo2){
			setTipoCombustivel(2);
			setRendimentoFinal(rendimento2);
			setCusto(custo2);
			System.out.println("o alcool e o combustivel e o custo e  " + custo2);
		}
		if(custo2>custo1){
			setTipoCombustivel(1);
			setRendimentoFinal(rendimento1);
			setCusto(custo1);
			System.out.println("a gasolina e o combustivel e o custo e  " + custo1);
		}
		System.out.println("O CUSTO DO CARRO E: " + Carro.getCusto());
	}
	@Override
	public void imprimirDados() {
		System.out.print("imprimir dados Carro oi");
		System.out.println("Combústivel é " + getCombustivel());
		System.out.println("Rendimento é " + getRendimento());
		System.out.println("Rendimento final é " + getRendimentoFinal());
		System.out.println("Carga máxima é " + getCargaMax());
		System.out.println("Velocidade media é " + getVelocidadeMedia());

	}
	

}
