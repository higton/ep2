package application;

import jdk.nashorn.internal.ir.Expression;

public class Moto extends Veiculo{
	private static double velocidadeMedia;
	private static double rendimentoFinal;
	private static double cargaMax;
	private static double custo;
	private static double tempo;
	private static boolean disponivel;
	private static int tipoCombustivel;

	//metodo acessores
	public static int getTipoCombustivel() { return tipoCombustivel; }
	public static void setTipoCombustivel(int tipoCombustivel) { Moto.tipoCombustivel = tipoCombustivel; }
	public static boolean getDisponivel() { return disponivel; }
	public static void setDisponivel(boolean disponivel) { Moto.disponivel = disponivel; }
	public static double getTempo() { return tempo; }
	public static void setTempo(double tempo) { Moto.tempo = tempo; }
	public static double getCusto() { return custo; }
	public static void setCusto(double custo) { Moto.custo = custo; }
	public static double getRendimentoFinal() {
		return rendimentoFinal;
	}
	public static double getCargaMax() {
		return cargaMax;
	}
	public static void setCargaMax(double cargaMax) {
		Moto.cargaMax = cargaMax;
	}
	public static void setRendimentoFinal(double rendimentoFinal) {
		Moto.rendimentoFinal = rendimentoFinal;
	}
	public static double getVelocidadeMedia() {
		return velocidadeMedia;
	}
	public static void setVelocidadeMedia(double velocidadeMedia) {
		Moto.velocidadeMedia = velocidadeMedia;
	} // fim dos metodos acessores
	
	
	public Moto() {
		setCargaMax(50);
		setVelocidadeMedia(110);
		setRendimentoFinal(0);
		
	}
	@Override
	public boolean excederTempo() {
		double v;
		v= ( Usuario.getDistanciaCarga()/Usuario.getTempoCarga() );
		if( v <= getVelocidadeMedia() ) {
			Moto.setDisponivel(true);
			return false;
		}
		else {
			System.out.println("excedeu o tempo");
			return true;
		}
	}
	@Override
	public boolean excederPeso() {
		if(getCargaMax()<Usuario.getPesoCarga()) {
			System.out.println("excedeu o peso");
			return true;
		}
		else {
			Moto.setDisponivel(true);
			return false;
		}
	}

	@Override
	public boolean disponivel(){

		if(Usuario.getQuantidadeMotos() != 0){
			Moto.setDisponivel(true);
			return true;
		}
		else{
			Moto.setDisponivel(false);
			return false;
		}
	}
	@Override
	public void calcularRendimentoFinal() {
		System.out.println("Passandoooo A MOTO");
		if(!excederTempo() && !excederPeso() && disponivel()){
			System.out.println("A MOTO NAO ESTA DISPONIVEL");
		}
		if(!excederTempo() && !excederPeso() && disponivel()){
			System.out.println("A MOTO ESTA DISPONIVEL");
		}
		double a = 0;
		double rendimento1 = 0;
		double rendimento2 = 0;
		double litros1, litros2, custo1, custo2;
		//Se o combustivel é gasolina
		if(!excederTempo() && !excederPeso() && disponivel()) {
			setCombustivel("Gasolina"); // combustivel 1
			setRendimento(50);
			a = Usuario.getPesoCarga();
			rendimento1 = getRendimento() - a*0.3;
		}
		//Se o combustível é álcool
		if(!excederTempo() && !excederPeso() && disponivel()) {
			setCombustivel("Alcool"); //combustivel 2
			setRendimento(43);
			a = Usuario.getPesoCarga();
			rendimento2 = getRendimento() - a*0.4;
		}
		//Escolhendo o combustível
		litros1 = Usuario.getDistanciaCarga()/rendimento1;
		litros2 = Usuario.getDistanciaCarga()/rendimento2;
		custo1 = litros1*getGasolina();
		custo2 = litros2*getAlcool();
		if(custo1 == 0 || custo2 == 0) {
			setTipoCombustivel(0);
		}
		if(custo1>custo2){
			setTipoCombustivel(2);
			setRendimentoFinal(rendimento2);
			setCusto(custo2);
			System.out.println("o alcool e o combustivel e o custo e  " + custo2);
		}
		if(custo2>custo1){
			setTipoCombustivel(1);
			setRendimentoFinal(rendimento1);
			setCusto(custo1);
			System.out.println("a gasolina e o combustivel e o custo e  " + custo1);
		}
	}

	@Override
	public void imprimirDados() {
		System.out.println("imprimir dados Moto");
		System.out.println("Combústivel é " + getCombustivel());
		System.out.println("Rendimento é " + getRendimento());
		System.out.println("Rendimento final é " + getRendimentoFinal());
		System.out.println("Carga máxima é " + getCargaMax());
		System.out.println("Velocidade media é " + getVelocidadeMedia());
	}
	

}
