package application;

public class Carreta extends Veiculo{
	private static double velocidadeMedia;
	private static double rendimentoFinal;
	private static double cargaMax;
	private static double custo;
	private static double tempo;
	private static boolean disponivel;
	private static int tipoCombustivel;

	//metodo acessores
	public static int getTipoCombustivel() { return tipoCombustivel; }
	public static void setTipoCombustivel(int tipoCombustivel) { Carreta.tipoCombustivel = tipoCombustivel; }
	public static boolean getDisponivel() { return disponivel; }
    public static void setDisponivel(boolean disponivel) { Carreta.disponivel = disponivel; }
	public static double getTempo() { return tempo; }
	public static void setTempo(double tempo) { Carreta.tempo = tempo; }
	public static double getCusto() { return custo; }
	public static void setCusto(double custo) { Carreta.custo = custo; }
	public static double getRendimentoFinal() {
		return rendimentoFinal;
	}
	public static double getCargaMax() {
		return cargaMax;
	}
	public static void setCargaMax(double cargaMax) {
		Carreta.cargaMax = cargaMax;
	}
	public static void setRendimentoFinal(double rendimentoFinal) {
		Carreta.rendimentoFinal = rendimentoFinal;
	}
	public static double getVelocidadeMedia() {
		return velocidadeMedia;
	}
	public static void setVelocidadeMedia(double velocidadeMedia) {
		Carreta.velocidadeMedia = velocidadeMedia;
	} // fim dos metodos acessores
	public Carreta() {
		setCombustivel("Diesel");
		setTipoCombustivel(0);
		setRendimento(8);
		setCargaMax(30000);
		setVelocidadeMedia(60);
		setRendimentoFinal(0);
	}
	public boolean excederPeso() {
		if(getCargaMax()<Usuario.getPesoCarga()) {
			return true;
		}
		else {
			Carreta.setDisponivel(true);
			return false;
		}
	}
	public boolean excederTempo() {
		double v;
		v= ( Usuario.getDistanciaCarga()/Usuario.getTempoCarga() );
		if( v <= getVelocidadeMedia() ) {
			Carreta.setDisponivel(true);
			return false;
		}
		else {
			return true;
		}
	}
	@Override
	public boolean disponivel(){

		if(Usuario.getQuantidadeCarretas() != 0){
			Carreta.setDisponivel(true);
			return true;
		}
		else{
			Carreta.setDisponivel(false);
			return false;
		}
	}
	@Override
	public void calcularRendimentoFinal() {
		double a;
		double f;
		a = Usuario.getPesoCarga();
		f = getRendimento() - a*0.0002;
		if(!excederTempo() && !excederPeso() && disponivel()) {
		setRendimentoFinal(f);
		}
	}

	@Override
	public void imprimirDados() {
		System.out.println("imprimir dados Carreta");
		System.out.println("Combústivel é " + getCombustivel());
		System.out.println("Rendimento é " + getRendimento());
		System.out.println("Rendimento final é " + getRendimentoFinal());
		System.out.println("Carga máxima é " + getCargaMax());
		System.out.println("Velocidade media é " + getVelocidadeMedia());
	}
	

}
