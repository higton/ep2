package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ControllerCena4 implements Initializable {
    @FXML private Text text1;
    @FXML private Text text2;
    @FXML private Text text3;
    @FXML private Text text4;
    @FXML private Text text5;
    @FXML private Text text6;
    @FXML private Text text7;
    @FXML private Text text8;
    @FXML private Text text9;
    @FXML private Text text10;
    @FXML private Text text11;
    @FXML private Text text12;
    @FXML private Text text13;
    @FXML private Text text14;
    @FXML private Text text15;
    @FXML private Text text16;
    @FXML private Text text17;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Terceira janela, gerar veiculos para colocar o resumo
            Calculo calculo = new Calculo();
            this.text9.setText("Mais rapido");
            if (calculo.calcularMenorCusto().equals("carro")) {
                this.text1.setText("Menor custo");
            }
            if (calculo.calcularMenorCusto().equals("moto")) {
                this.text2.setText("Menor custo");
            }
            if (calculo.calcularMenorCusto().equals("van")) {
                this.text3.setText("Menor custo");
            }
            if (calculo.calcularMenorCusto().equals("carreta")) {
                this.text4.setText("Menor custo");
            }
            if (calculo.calcularCustoBeneficio().equals("carro")) {
                this.text5.setText("Maior custo beneficio");
            }
            if (calculo.calcularCustoBeneficio().equals("moto")) {
                this.text6.setText("Maior custo beneficio");
            }
            if (calculo.calcularCustoBeneficio().equals("van")) {
                this.text7.setText("Maior custo beneficio");
            }
            if (calculo.calcularCustoBeneficio().equals("carreta")) {
                this.text8.setText("Maior custo beneficio");
            }
            //Arredonda o valor para caber no GUI
            int a = (int) Math.round(Carro.getCusto() * (Usuario.getMargemLucro() + 1));
            int b = (int) Math.round(Moto.getCusto() * (Usuario.getMargemLucro() + 1));
            int c = (int) Math.round(Van.getCusto() * (Usuario.getMargemLucro() + 1));
            int d = (int) Math.round(Carreta.getCusto() * (Usuario.getMargemLucro() + 1));
            int e = (int) Math.round(Carro.getTempo());
            int f = (int) Math.round(Moto.getTempo());
            int g = (int) Math.round(Van.getTempo());
            int h = (int) Math.round(Carreta.getTempo());

            this.text10.setText("" + a);
            this.text11.setText("" + b);
            this.text12.setText("" + c);
            this.text13.setText("" + d);
            this.text14.setText("" + e);
            this.text15.setText("" + f);
            this.text16.setText("" + g);
            this.text17.setText("" + h);
    }
    //Funções para verificar se o veiculo está disponível
    @FXML
    private void addScenePeloCarro(ActionEvent event) throws IOException {
        if (!Carro.getDisponivel()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Alerta");
            alert.setHeaderText("Falha na escolha do veiculo");
            alert.setContentText("Você tentou clicar em um veiculo não disponivel");
            alert.show();
        }
        if (Carro.getDisponivel()) {
            //atualizar o valor da demanda
            Usuario.setVeiculoEscolhido("carro");
            Usuario.setNumeroDemanda(Usuario.getNumeroDemanda() + 1);
            Parent view3 = FXMLLoader.load(getClass().getResource("cena5.fxml"));
            Scene scene3 = new Scene(view3);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setScene(scene3);
            window.show();
        }
    }

    @FXML
    private void addScenePelaMoto(ActionEvent event) throws IOException {
        if (!Moto.getDisponivel()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Alerta");
            alert.setHeaderText("Falha na escolha do veiculo");
            alert.setContentText("Você tentou clicar em um veiculo não disponivel");
            alert.show();
        }
        if (Moto.getDisponivel()) {
            //atualizar o valor da demanda
            Usuario.setVeiculoEscolhido("moto");
            Usuario.setNumeroDemanda(Usuario.getNumeroDemanda() + 1);
            Parent view3 = FXMLLoader.load(getClass().getResource("cena5.fxml"));
            Scene scene3 = new Scene(view3);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setScene(scene3);
            window.show();
        }
    }

    @FXML
    private void addScenePelaVan(ActionEvent event) throws IOException {
        if (!Van.getDisponivel()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Alerta");
            alert.setHeaderText("Falha na escolha do veiculo");
            alert.setContentText("Você tentou clicar em um veiculo não disponivel");
            alert.show();
        }
        if (Van.getDisponivel()) {
            //atualizar o valor da demanda
            Usuario.setVeiculoEscolhido("van");
            Usuario.setNumeroDemanda(Usuario.getNumeroDemanda() + 1);
            Parent view3 = FXMLLoader.load(getClass().getResource("cena5.fxml"));
            Scene scene3 = new Scene(view3);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setScene(scene3);
            window.show();
        }
    }

    @FXML
    private void addScenePelaCarreta(ActionEvent event) throws IOException {
        if (!Carreta.getDisponivel()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Alerta");
            alert.setHeaderText("Falha na escolha do veiculo");
            alert.setContentText("Você tentou clicar em um veiculo não disponivel");
            alert.show();
        }
        if (Carreta.getDisponivel()) {
            //atualizar o valor da demanda
            Usuario.setVeiculoEscolhido("carreta");
            Usuario.setNumeroDemanda(Usuario.getNumeroDemanda() + 1);
            Parent view3 = FXMLLoader.load(getClass().getResource("cena5.fxml"));
            Scene scene3 = new Scene(view3);
            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setScene(scene3);
            window.show();
        }
    }
    public void voltarCena2(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("cena3.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }
}

