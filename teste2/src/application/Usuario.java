package application;

public class Usuario {

	public Usuario() {
		// TODO Auto-generated constructor stub
	}
	
	private static double quantidadeCarros;
	private static double quantidadeVans;
	private static double quantidadeCarretas;
	private static double quantidadeMotos;
	private static double pesoCarga;
	private static double distanciaCarga;
	private static double tempoCarga;
	private static double menorCusto;
	private static double margemLucro;
	private static int numeroDemanda;
	private static String veiculoEscolhido;

	public static String getVeiculoEscolhido() { return veiculoEscolhido; }
	public static void setVeiculoEscolhido(String veiculoEscolhido) { Usuario.veiculoEscolhido = veiculoEscolhido; }
	public static int getNumeroDemanda() { return numeroDemanda; }
	public static void setNumeroDemanda(int numeroDemanda) { Usuario.numeroDemanda = numeroDemanda; }
	public static double getMargemLucro() { return margemLucro;}
	public static void setMargemLucro(double margemLucro) { Usuario.margemLucro = margemLucro; }
	public static double getMenorCusto() { return menorCusto; }
	public static void setMenorCusto(double menorCusto) { Usuario.menorCusto = menorCusto; }
	public static void removerCarreta() {
		Usuario.setQuantidadeCarretas(Usuario.getQuantidadeCarretas()-1);
	}
	public static void removerCarro() {
		Usuario.setQuantidadeCarros(Usuario.getQuantidadeCarros()-1);
	}
	public static void removerVan() {
		Usuario.setQuantidadeVans(Usuario.getQuantidadeVans()-1);
	}
	public static void removerMoto() {
		Usuario.setQuantidadeMotos(Usuario.getQuantidadeMotos()-1);
	}
	public static void adicionarCarreta() {
		Usuario.setQuantidadeCarretas(Usuario.getQuantidadeCarretas()+1);
	}
	public static void adicionarCarro() {
		Usuario.setQuantidadeCarros(Usuario.getQuantidadeCarros()+1);
	}
	public static void adicionarVan() {
		Usuario.setQuantidadeVans(Usuario.getQuantidadeVans()+1);
	}
	public static void adicionarMoto() {
		Usuario.setQuantidadeMotos(Usuario.getQuantidadeMotos()+1);
	}
	public static double getQuantidadeCarros() {
		return quantidadeCarros;
	}
	public static void setQuantidadeCarros(double quantidadeCarros) {
		Usuario.quantidadeCarros = quantidadeCarros;
	}
	public static double getQuantidadeVans() {
		return quantidadeVans;
	}
	public static void setQuantidadeVans(double quantidadeVans) {
		Usuario.quantidadeVans = quantidadeVans;
	}
	public static double getQuantidadeCarretas() {
		return quantidadeCarretas;
	}
	public static void setQuantidadeCarretas(double quantidadeCarretas) { Usuario.quantidadeCarretas = quantidadeCarretas; }
	public static double getQuantidadeMotos() {
		return quantidadeMotos;
	}
	public static void setQuantidadeMotos(double quantidadeMotos) { Usuario.quantidadeMotos = quantidadeMotos; }
	public static double getPesoCarga() { return pesoCarga; }
	public static void setPesoCarga(double pesoCarga) {
		Usuario.pesoCarga = pesoCarga;
	}
	public static double getDistanciaCarga() {
		return distanciaCarga;
	}
	public static void setDistanciaCarga(double distanciaCarga) {
		Usuario.distanciaCarga = distanciaCarga;
	}
	public static double getTempoCarga() {
		return tempoCarga;
	}
	public static void setTempoCarga(double tempoCarga) {
		Usuario.tempoCarga = tempoCarga;
	}

}
