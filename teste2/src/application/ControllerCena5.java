package application;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ControllerCena5 implements Initializable {
    @FXML
    private ListView listView;

    @Override
    public void initialize(URL url, ResourceBundle rb){
        listView.setItems(listItems);
        //mudando o tamanho da letra no listview
        listView.setStyle("-fx-font-size: 20px; -fx-font-family: 'SketchFlow Print';");
        //These items are for configuring the ListArea
        System.out.println("lendo arquivo");
        try {
            Arquivo.escreverArquivoDemanda();

            BufferedReader reader = new BufferedReader(new FileReader("demandas.txt"));
            String line = reader.readLine();
            int i = 0;
            while (line != null) {
                i++;
                line.trim().startsWith("0.0");
                String[] data = line.split(","); // since your delimiter is ","
                if (data[0] != "0.0") {
                    System.out.println(data[0]);
                    listView.getItems().add("Distancia: " + data[0] + " kilometros, peso da carga: " + data[1] + " kilogramas , tempo: "
                            + data[2] + " horas, " + "margem de lucro: " + data[3] + "%, o veículo escolhido é: " + data[4]);
                    //ler a proxima linha

                    line = reader.readLine();
                }
            }

            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    final ObservableList<String> listItems = FXCollections.observableArrayList("Lista: ");

}

