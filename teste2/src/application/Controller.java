package application;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.fxml.FXML;
import javafx.scene.Node;


public class Controller {
    @FXML //fx:id="text-field";
    private TextField text_field1;
    @FXML private TextField text_field2;
    @FXML private TextField text_field3;
    @FXML
    private void goScene4(ActionEvent event) throws IOException {
        int flag = Usuario.getNumeroDemanda() + 1;
        Usuario.setNumeroDemanda(flag);

        String palavra1 = this.text_field1.getText();
        String palavra2 = this.text_field2.getText();
        String palavra3 = this.text_field3.getText();

        //Ler os dados colocados e calcular para a próxima cena
        double distancia, peso, tempo;
        if (!palavra1.equals("") && !palavra2.equals("") && !palavra3.equals("")) {
            Arquivo.lerArquivoDemanda();

            distancia = Double.parseDouble(palavra1);
            peso = Double.parseDouble(palavra2);
            tempo = Double.parseDouble(palavra3);
            Usuario.setDistanciaCarga(distancia);
            Usuario.setPesoCarga(peso);
            Usuario.setTempoCarga(tempo);
            Veiculo.setAlcool(3.499);
            Veiculo.setDiesel(3.869);
            Veiculo.setGasolina(4.449);
            Carro carro = new Carro(360, 100, 0);
            Moto moto = new Moto();
            Van van = new Van();
            Carreta carreta = new Carreta();
            Calculo calculo = new Calculo();
            carro.calcularRendimentoFinal();
            moto.calcularRendimentoFinal();
            carreta.calcularRendimentoFinal();
            van.calcularRendimentoFinal();
            calculo.calcularCusto();
            calculo.calcularMenorCusto();
            calculo.calcularMaisRapido();
            calculo.calcularCustoBeneficio();

            System.out.println("O NUMERO DE MOTOS DISPONIVEIS É : " + Usuario.getQuantidadeMotos() );

            Parent view3 = FXMLLoader.load(getClass().getResource("cena4.fxml"));

            Scene scene3 = new Scene(view3);

            Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();
            window.setScene(scene3);
            window.show();
        }

    }

    public void voltarCena1(ActionEvent event) throws IOException {
        Parent tableViewParent = FXMLLoader.load(getClass().getResource("cena1.fxml"));
        Scene tableViewScene = new Scene(tableViewParent);

        //This line gets the Stage information
        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(tableViewScene);
        window.show();
    }
}

    /*public void listViewButtonPushed() {
        String textAreaString = "";

        ObservableList listOfItems = listView.getSelectionModel().getSelectedItems();

        for (Object item : listOfItems) {
            textAreaString += String.format("%s%n", (String) item);
        }

        //this.golfTextArea.setText(textAreaString);
    }*/

    //These items are for configuring the ListArea
