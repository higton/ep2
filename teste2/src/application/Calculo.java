package application;
import java.lang.*;

public class Calculo {
	public Calculo() {

	}

	//imprime as informações dos veiculos
	public void status(double c, double d) {

		System.out.println("\nO tempo que levará para ele realizar a entrega ");
		System.out.printf("%.2f", c);
		System.out.println("\nA margem de lucro é ");
		System.out.printf("%.2f\n", d);
	}

	public void calcularCusto() {

		System.out.println("calculando custo");

		double litrosCarreta = Usuario.getDistanciaCarga() / Carreta.getRendimentoFinal();
		double litrosMoto = Usuario.getDistanciaCarga() / Moto.getRendimentoFinal();
		double litrosVan = Usuario.getDistanciaCarga() / Van.getRendimentoFinal();
		double litrosCarro = Usuario.getDistanciaCarga() / Carro.getRendimentoFinal();

		double carretaTempo = Usuario.getDistanciaCarga() / Carreta.getVelocidadeMedia();
		double motoTempo = Usuario.getDistanciaCarga() / Moto.getVelocidadeMedia();
		double vanTempo = Usuario.getDistanciaCarga() / Van.getVelocidadeMedia();
		double carroTempo = Usuario.getDistanciaCarga() / Carro.getVelocidadeMedia();
		Moto.setTempo(motoTempo);
		Carro.setTempo(carroTempo);
		Carreta.setTempo(carretaTempo);
		Van.setTempo(vanTempo);

		double motoCusto = 0;
		double carroCusto = 0;
		double vanCusto = litrosVan * Veiculo.getDiesel();
		double carretaCusto = litrosCarreta * Veiculo.getDiesel();

		//Depende do tipo do combustivel um custo diferente
		if (Moto.getTipoCombustivel() == 1) {
			motoCusto = litrosMoto * Veiculo.getGasolina();
			double motoCustoBeneficio = motoCusto * motoTempo;
		}
		if (Moto.getTipoCombustivel() == 2) {
			motoCusto = litrosMoto * Veiculo.getAlcool();
			double motoCustoBeneficio = motoCusto * motoTempo;
		}
		if (Carro.getTipoCombustivel() == 1) {
			carroCusto = litrosCarro * Veiculo.getGasolina();
			double carroCustoBeneficio = carroCusto * carroTempo;
		}

		if (Carro.getTipoCombustivel() == 2) {
			carroCusto = litrosCarro * Veiculo.getAlcool();
			double carroCustoBeneficio = carroCusto * carroTempo;
		}

		Moto.setCusto(motoCusto);
		Carro.setCusto(carroCusto);
		Van.setCusto(vanCusto);
		Carreta.setCusto(carretaCusto);
	}

	public String calcularMenorCusto() {
		double carretaTempo = Usuario.getDistanciaCarga() / Carreta.getVelocidadeMedia();
		double motoTempo = Usuario.getDistanciaCarga() / Moto.getVelocidadeMedia();
		double vanTempo = Usuario.getDistanciaCarga() / Van.getVelocidadeMedia();
		double carroTempo = Usuario.getDistanciaCarga() / Carro.getVelocidadeMedia();
		double menor = 999999;
		String s = new String("moto");

		if ( ( Moto.getCusto() < menor ) && Moto.getDisponivel() ) {
			menor = Moto.getCusto();
		}
		if ( ( Carro.getCusto() < menor ) && Carro.getDisponivel() ) {
			menor = Carro.getCusto();
		}
		if ( ( Carreta.getCusto() < menor ) && Carreta.getDisponivel() ) {
			menor = Carreta.getCusto();
		}
		if ( ( Van.getCusto() < menor ) && Van.getDisponivel() ) {
			menor = Van.getCusto();
		}
		if (menor == Moto.getCusto()) {
			s = "moto";
			System.out.println("A moto tem o menor custo, o seu custo de operação é ");
			System.out.printf("%.2f", Moto.getCusto());
			Usuario.setMenorCusto(Moto.getCusto());
			status(motoTempo, Moto.getCusto());
		}
		if (menor == Carro.getCusto()) {
			System.out.println("O carro tem o menor custo, o seu custo de operação é ");
			s = "carro";
			System.out.printf("%.2f", Carro.getCusto());
			status(carroTempo, Carro.getCusto());
			Usuario.setMenorCusto(Carro.getCusto());
		}
		if (menor == Carreta.getCusto()) {
			s = "carreta";
			System.out.println("Carreta tem o menor custo , o seu custo é ");
			System.out.printf("%.2f", Carreta.getCusto());
			status(carretaTempo, Carreta.getCusto());
		}
		if (menor == Van.getCusto()) {
			s = "van";
			System.out.println("\nVan tem o maior custo-benefício, o seu custo-benefício é ");
			System.out.printf("%.2f", Van.getCusto());
			status(vanTempo, Van.getCusto());
		}
		return s;
	}

	public String calcularMaisRapido() {
		double v1, v2, v3, v4, veiculo2, tempo2;
		v1 = Carreta.getVelocidadeMedia();
		v2 = Moto.getVelocidadeMedia();
		v3 = Van.getVelocidadeMedia();
		v4 = Carro.getVelocidadeMedia();

		tempo2 = Usuario.getDistanciaCarga() / Moto.getVelocidadeMedia();
		veiculo2 = Usuario.getDistanciaCarga() / Moto.getRendimentoFinal();

		if (v2 > v1 && v2 > v3 && v2 > v4 && Moto.getDisponivel() && Moto.getTipoCombustivel() == 1) {
			System.out.printf("A moto é a mais rapida");
			status(tempo2, veiculo2 * Moto.getGasolina());
		}
		if (v2 > v1 && v2 > v3 && v2 > v4 && Moto.getDisponivel() && Moto.getTipoCombustivel() == 2) {
			System.out.printf("A moto é a mais rapida");
			status(tempo2, veiculo2 * Moto.getAlcool());
		}
		String s = new String("moto");
		return s;
	}


	public String calcularCustoBeneficio() {
		String escolhido = new String("moto");
		double menor = 999999;

		double carretaCustoBeneficio = Carreta.getCusto() * Carreta.getTempo();
		double vanCustoBeneficio = Van.getCusto() * Van.getTempo();
		double motoCustoBeneficio = Moto.getCusto() * Moto.getTempo();
		double carroCustoBeneficio = Carro.getCusto() * Carro.getTempo();

		if ( ( motoCustoBeneficio < menor ) && Moto.getDisponivel() ) {
			menor = motoCustoBeneficio;
		}
		if ( ( carroCustoBeneficio < menor ) && Carro.getDisponivel() ) {
			menor = carroCustoBeneficio;
		}
		if ( ( carretaCustoBeneficio < menor ) && Carreta.getDisponivel() ) {
			menor = carretaCustoBeneficio;
		}
		if ( ( vanCustoBeneficio < menor ) && Van.getDisponivel() ) {
			menor = vanCustoBeneficio;
		}
		if (menor == motoCustoBeneficio) {
			escolhido = "moto";
			System.out.println("Moto tem o maior custo-benefício, o seu custo-beneficio é ");
			System.out.printf("%.2f", Moto.getCusto() / Moto.getCusto());
			System.out.println("\nO seu custo é ");
			System.out.printf("%.2f", Moto.getCusto());
			status(Moto.getTempo(), Moto.getCusto());
		}
		if (menor == carroCustoBeneficio) {
			escolhido = "carro";
			System.out.println("Carro tem o maior custo-benefício, o seu custo-beneficio é ");
			System.out.printf("%.2f", Carro.getCusto() / Carro.getTempo());
			System.out.println("\nO seu custo é ");
			System.out.printf("%.2f", Carro.getCusto());
			status(Carro.getTempo(), Carro.getCusto());
		}
		if (menor == carretaCustoBeneficio) {
			escolhido = "carreta";
			System.out.println("Carreta tem o menor custo , o seu custo é ");
			System.out.printf("%.2f", Carreta.getCusto());
			status(Carreta.getTempo(), Carreta.getCusto());
		}
		if (menor == vanCustoBeneficio) {
			escolhido = "van";
			System.out.println("Moto tem o maior custo-benefício, o seu custo-beneficio é ");
			System.out.printf("%.2f", Van.getCusto()/ Van.getTempo());
			System.out.println("\nO seu custo é ");
			System.out.printf("%.2f", Van.getCusto());
			status(Van.getTempo(), Van.getCusto());
		}


		return escolhido;
	}
}


			//Dependo do tipo do combustivel um custo diferente
		/*if(Usuario.getCombustivel() == 1) {
			veiculo2=veiculo2* Veiculo.getGasolina();
			veiculo4=veiculo4* Veiculo.getGasolina();
			veiculo3=0;
			veiculo1=0;
			if(tempo2 < tempo4 && veiculo2 < veiculo4 && Moto.excederPeso()==false){
				System.out.println("Moto tem o maior custo-benefício, o seu custo-beneficio é ");
				System.out.printf("%.2f", veiculo2/tempo2);
			}
			if(tempo4 < tempo2 && veiculo4 < veiculo2 && Carro.excederPeso()==false){
				System.out.println("\nCarro tem o maior custo-benefício, o seu custo-beneficio é ");
				System.out.printf("%.2f", veiculo4/tempo4);
			}
		}
		if(Usuario.getCombustivel() == 2) { 
			veiculo2=veiculo2* Veiculo.getAlcool();
			veiculo4=veiculo4* Veiculo.getAlcool();
			veiculo3=0;
			veiculo1=0;if(tempo2 < tempo4 && veiculo2 < veiculo4 && Moto.excederPeso()==false){
				System.out.println("Moto tem o maior custo-benefício, o seu custo-beneficio é ");
				System.out.printf("%.2f", veiculo2/tempo2);
			}
			if(tempo4 < tempo2 && veiculo4 < veiculo2 && Carro.excederPeso()==false){
				System.out.println("\nCarro tem o maior custo-benefício, o seu custo-beneficio é ");
				System.out.printf("%.2f", veiculo4/tempo4);
			}
		}
		if(Usuario.getCombustivel() == 0) {
			veiculo1=veiculo1*Veiculo.getDiesel();
			veiculo3=veiculo3*Veiculo.getDiesel();
			veiculo2=0;
			veiculo4=0;
			if(tempo1 < tempo3 && veiculo1 < veiculo3 && Carreta.excederPeso()==false){
				System.out.println("Carreta tem o maior custo-benefício, o seu custo de operação é ");
				System.out.printf("%.2f", veiculo1/tempo1);
				
			}
			if(tempo3 < tempo1 && veiculo3 < veiculo1 && Van.excederPeso()==false){
				System.out.println("\nVan tem o maior custo-benefício, o seu custo de operação é ");
				System.out.printf("%.2f", veiculo3/tempo3);
				
			}
		}*/


	/*//Dependo do tipo do combustivel um custo diferente
	if(Usuario.getCombustivel() == 1) {
		veiculo2=veiculo2* Veiculo.getGasolina();
		veiculo4=veiculo4* Veiculo.getGasolina();
		veiculo3=0;
		veiculo1=0;
		if(veiculo2 < veiculo4 && Moto.excederPeso()==false){
			System.out.println("Moto tem o menor custo, o seu custo de operação é " + veiculo2);
			System.out.println("O tempo que ele levará para ele realizar a entrega " + tempo2);
			System.out.println("A margem de lucro é " + (Usuario.getPrecoVenda()-veiculo2) );
		}
		if(veiculo4 < veiculo2 && Carro.excederPeso()==false){
			System.out.println("Carro tem o menor custo, o seu custo de operação é " + veiculo4);
			System.out.println("O tempo que ele levará para ele realizar a entrega " + tempo4);
			System.out.println("A margem de lucro é " + (Usuario.getPrecoVenda()-veiculo4) );

		}
	}
	if(Usuario.getCombustivel() == 2) { 
		veiculo2=veiculo2* Veiculo.getAlcool();
		veiculo4=veiculo4* Veiculo.getAlcool();
		veiculo3=0;
		veiculo1=0;
		if(veiculo2 < veiculo4 && Moto.excederPeso()==false){
			System.out.println("Moto tem o menor custo, o seu custo de operação é " + veiculo2);
			System.out.println("O tempo que ele levará para ele realizar a entrega " + tempo2);
			System.out.println("A margem de lucro é " + (Usuario.getPrecoVenda()-veiculo2) );
		}
		if(veiculo4 < veiculo2 && Carro.excederPeso()==false){
			System.out.println("Carro tem o menor custo, o seu custo de operação é " + veiculo4);
			System.out.println("O tempo que ele levará para ele realizar a entrega " + tempo4);
			System.out.println("A margem de lucro é " + (Usuario.getPrecoVenda()-veiculo4) );
		}
	}
	if(Usuario.getCombustivel() == 3) {
		veiculo1=veiculo1* Veiculo.getDiesel();
		veiculo3=veiculo3* Veiculo.getDiesel();
		veiculo2=0;
		veiculo4=0;
		if(veiculo1 < veiculo3 && Carreta.excederPeso()==false){
			System.out.println("Carreta tem o menor custo, o seu custo de operação é " + veiculo1);
			System.out.println("O tempo que ele levará para ele realizar a entrega " + tempo1);
		}
		if(veiculo3 < veiculo1 && Van.excederPeso()==false){
			System.out.println("Van tem o menor custo, o seu custo de operação é " + veiculo3);
			System.out.println("O tempo que ele levará para ele realizar a entrega " + tempo3);
		}
	}
	
	System.out.println("O valor de veiculo1(carreta) e " + veiculo1);
	System.out.println("O valor de veiculo2(moto) é " + veiculo2);
	System.out.println("O valor de veiculo3(van) é " + veiculo3);
	System.out.println("O valor de veiculo4(carro) é " + veiculo4);
	
}*/
