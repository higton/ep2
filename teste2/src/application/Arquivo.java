package application;

import java.io.*;
import java.util.*;

public class Arquivo {

    private static Formatter output; // envia uma saída de texto para um arquivo
    private static Scanner input;

    // lê o registro no arquivo
    public static void lerArquivo() {
        System.out.println("lendo arquivo");
        try {
            BufferedReader reader = new BufferedReader(new FileReader("clients.txt"));
            String line = reader.readLine();
            List<String> listaTeste = new ArrayList<>();
            int i=0;
            while (line != null) {
                listaTeste.add(line);
                List<String> l = listaTeste;
                System.out.println(listaTeste.get(i));
                i++;
                String[] data = line.split(","); // since your delimiter is ","
                Usuario.setQuantidadeMotos(Double.parseDouble(data[0]));
                Usuario.setQuantidadeCarros(Double.parseDouble(data[1]));
                Usuario.setQuantidadeCarretas(Double.parseDouble(data[2]));
                Usuario.setQuantidadeVans(Double.parseDouble(data[3]));
                Usuario.setMargemLucro(Double.parseDouble(data[4]));
                Usuario.setNumeroDemanda(Integer.parseInt(data[5]));

                System.out.println("Quantidade de motos pelo arquivo e" + Usuario.getQuantidadeMotos());
                System.out.println("Quantidade de carretas pelo arquivo e " + Usuario.getQuantidadeCarretas());
                System.out.println("A margem de lucro pelo arquivo e " + Usuario.getMargemLucro());
                System.out.println(" corta ");
                //ler a proxima linha
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    } // fim do método lerArquivo

    // adiciona registros ao arquivo
    public static void escreverArquivo() throws IOException {
        try(FileWriter fw = new FileWriter("clients.txt",true))
        {
             //the true will append the new data
            fw.write(Usuario.getQuantidadeMotos() + "," + Usuario.getQuantidadeCarros() + "," + Usuario.getQuantidadeCarretas() + "," +
                    Usuario.getQuantidadeVans()+ "," + Usuario.getMargemLucro() + "," + Usuario.getNumeroDemanda() + "\n");//appends the string to the file
        }
        catch(IOException ioe)
        {
            System.err.println("IOException: " + ioe.getMessage());
        }

    }
    public static void escreverArquivoDemanda() throws IOException {
            FileWriter fw = new FileWriter("demandas.txt",true); //the true will append the new data
            fw.write(Usuario.getDistanciaCarga() + "," + Usuario.getPesoCarga() + "," + Usuario.getTempoCarga() + "," +
                     + Usuario.getMargemLucro() + "," + Usuario.getVeiculoEscolhido() + ","
                     + Usuario.getNumeroDemanda()+ "\n");//appends the string to the file
            fw.close();

    }
    public static void lerArquivoDemanda() {
        System.out.println("lendo arquivo demanda");
        try {
            BufferedReader reader = new BufferedReader(new FileReader("demandas.txt"));
            String line = reader.readLine();
            int i = 0;
            while (line != null) {
                i++;
                String[] data = line.split(","); // since your delimiter is ","
                //Vendo os veiculos no arquivo que estão ocupados com a demanda
                if(data[4].equals("moto")){
                    Usuario.setQuantidadeMotos( Usuario.getQuantidadeMotos() - 1 );
                }
                if(data[4].equals("carro")){
                    Usuario.setQuantidadeCarros( Usuario.getQuantidadeCarros() - 1 );
                }
                if(data[4].equals("carreta")){
                    Usuario.setQuantidadeCarretas( Usuario.getQuantidadeCarretas() - 1 );
                }
                if(data[4].equals("van")){
                    Usuario.setQuantidadeVans( Usuario.getQuantidadeVans() - 1 );
                }

                System.out.println("Quantidade de motos pelo arquivo demanda e" + Usuario.getQuantidadeMotos());
                System.out.println("Quantidade de carretas pelo arquivo demanda e " + Usuario.getQuantidadeCarretas());
                System.out.println("A margem de lucro pelo arquivo demanda e " + Usuario.getMargemLucro());
                System.out.println(" corta ");
                //ler a proxima linha
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    } // fim do método lerArquivo
    public static void delete(int lineToRemove) throws IOException {
        File inputFile = new File("demandas.txt");
        File tempFile = new File("demandasTemp.txt");

        BufferedReader reader = new BufferedReader(new FileReader(inputFile));
        BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));

        String currentLine;
        int count = 0;

        while ((currentLine = reader.readLine()) != null) {
            count++;
            if (count == lineToRemove) {
                continue;
            }
            if (count != lineToRemove){
                writer.write(currentLine + System.getProperty("line.separator"));
            }
        }
        writer.close();
        reader.close();
        inputFile.delete();
        tempFile.renameTo(inputFile);
    }

}
