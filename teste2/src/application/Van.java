package application;

public class Van extends Veiculo{
	private static double velocidadeMedia;
	private static double rendimentoFinal;
	private static double cargaMax;
	private static double custo;
	private static double tempo;
	private static boolean disponivel;
	private static int tipoCombustivel;

	//metodo acessores
	public static int getTipoCombustivel() { return tipoCombustivel; }
	public static void setTipoCombustivel(int tipoCombustivel) { Van.tipoCombustivel = tipoCombustivel; }
	public static boolean getDisponivel() { return disponivel; }
	public static void setDisponivel(boolean disponivel) { Van.disponivel = disponivel; }
	public static double getTempo() { return tempo; }
	public static void setTempo(double tempo) { Van.tempo = tempo; }
	public static double getCusto() { return custo; }
	public static void setCusto(double custo) { Van.custo = custo; }
	public static double getRendimentoFinal() {
		return rendimentoFinal;
	}
	public static double getCargaMax() {
		return cargaMax;
	}
	public static void setCargaMax(double cargaMax) {
		Van.cargaMax = cargaMax;
	}
	public static void setRendimentoFinal(double rendimentoFinal) {
		Van.rendimentoFinal = rendimentoFinal;
	}
	public static double getVelocidadeMedia() {
		return velocidadeMedia;
	}
	public static void setVelocidadeMedia(double velocidadeMedia) {
		Van.velocidadeMedia = velocidadeMedia;
	} // fim dos metodos acessores
	
	
	public Van() {
		setCombustivel("Diesel");
		setRendimento(10);
		setCargaMax(3500);
		setVelocidadeMedia(80);
		setRendimentoFinal(0);
		setTipoCombustivel(0);
	}
	@Override
	public boolean excederTempo() {
		double v;
		v= ( Usuario.getDistanciaCarga()/Usuario.getTempoCarga() );
		if( v <= getVelocidadeMedia() ) {
			Van.setDisponivel(true);
			return false;
		}
		else {
			Van.setDisponivel(false);
			return true;
		}
	}
	@Override
	public boolean excederPeso() {
		if(getCargaMax()<Usuario.getPesoCarga()) {
			Van.setDisponivel(false);
			return true;
		}
		else {
			Van.setDisponivel(true);
			return false;
		}
	}
	@Override
	public boolean disponivel(){

		if(Usuario.getQuantidadeVans() != 0){
			Van.setDisponivel(true);
			return true;
		}
		else{
			Van.setDisponivel(false);
			return false;
		}
	}
	@Override
	public void calcularRendimentoFinal() {

		double a;
		double f;
		a = Usuario.getPesoCarga();
		f = getRendimento() - a*0.001;
		if(!excederPeso() && !excederTempo() && disponivel()) {
		setRendimentoFinal(f);
		}
	}
	@Override
	public void imprimirDados() {
		System.out.println("imprimir dados Van");
		System.out.println("Combústivel é " + getCombustivel());
		System.out.println("Rendimento é " + getRendimento());
		System.out.println("Rendimento final é " + getRendimentoFinal());
		System.out.println("Carga máxima é " + getCargaMax());
		System.out.println("Velocidade media é " + getVelocidadeMedia());
	}
	

}
