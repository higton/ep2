package application;

public abstract class Veiculo {
		// Atributos de Veiculo
		private String combustivel;
		private double rendimento;
		private static double alcool;
		private static double gasolina;
		private static double diesel;


		// Métodos de Veículo
		public abstract void calcularRendimentoFinal();
		public abstract boolean disponivel();
		public abstract void imprimirDados();
		public abstract boolean excederPeso();
		public abstract boolean excederTempo();
		//Métodos acessores
		public String getCombustivel() {
			return combustivel;
		}
		public void setCombustivel(String combustivel) {
			this.combustivel = combustivel;
		}
		public double getRendimento() {
			return rendimento;
		}
		public void setRendimento(double rendimento) {
			this.rendimento = rendimento;
		}
		public static double getAlcool() {
			return alcool;
		}
		public static void setAlcool(double alcool) {
			Veiculo.alcool = alcool;
		}
		public static double getGasolina() {
			return gasolina;
		}
		public static void setGasolina(double gasolina) {
			Veiculo.gasolina = gasolina;
		}
		public static double getDiesel() {
			return diesel;
		}
		public static void setDiesel(double diesel) {
			Veiculo.diesel = diesel;
		}
		
	
}
